<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Mahasiswa</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>
    <table border="1" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
            </tr>
        </thead>
        <tbody>
            <?php
                include "koneksi.php";

                $sql = mysqli_query($koneksi,"SELECT * FROM mahasiswa");

                $no=1;
                while ($data=mysqli_fetch_array($sql)) {
                    echo "<tr>
                        <td>$no</td>
                        <td>$data[nim]</td>
                        <td>$data[nama]</td>
                    </tr>";

                    $no++;
                }
            ?>
        </tbody>
    </table>
</body>
</html>